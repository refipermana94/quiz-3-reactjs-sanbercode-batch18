import React from "react";
import "./Routes.css"
//import "./style.css"

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
// import Tugas9 from "../Tugas-9/formBeli"
// import Tugas10 from "../Tugas-10/tabel"
// import Tugas11 from "../Tugas-11/Timer"
// import Tugas12 from "../Tugas-12/DaftarBuah"
// import Tugas13 from "../Tugas-13/DaftarBuah"
// import Tugas14 from "../Tugas-14/DaftarBuah"
// import Toogle from"../Toogle/Toogle"
import Home1 from "../Home/Home"
import About1 from "../About/About"
import Login1 from "../Login/Login"
import "./Routes.css"

const Routes = () => {
  return (
    <Router>
      <div>
        <nav>
          <ul>
           
            <li style={{float:"right"}}>
              <Link to="/Login">Login</Link>
            </li>
             
            <li style={{float:"right"}}>
              <Link to="/About">About</Link>
            </li>
            <li style={{float:"right"}}>
              <Link to="/">Home</Link>
            </li>
           
            
          </ul>
           <Link to="/" >
                  
                </Link>
        </nav>
        <Switch>
          <Route exact path="/"><Home1 /></Route>
          <Route exact path="/About" component={About1}></Route>
          <Route exact path="/Login" component={Login1}></Route>
          

        </Switch>
      </div>
    </Router>
  );
}


export default Routes